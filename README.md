# Sentiment Analysis

## This is Data Mining CIT653-Spring 2019 Project
- The project report will be in: https://docs.google.com/document/d/1S7mB91kkkmXGM75KEsRztHM6hxmPFL32xY1RSac1HEU/edit?usp=sharing
- Project Description: Term_Project.pdf
- Data: inside the data folder.
- Resources: Contains the list of stop words, list of opinion words in opinion-lexicon, another file I don't know what it is.
- Papers: contains relevant research papers to the algorithms and the data.

## Software Requirements:
- Python 3
- pip
- Linux OS (if you are not using Linux you may have problems with realtive paths)

### To install other requirements
```
pip3 install -r requirements.txt
```