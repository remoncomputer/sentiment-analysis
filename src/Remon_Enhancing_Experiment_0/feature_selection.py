import os

from preprocessing import preprocess_sentence_ekphrasis, tokenize_sentence_ekphrasis_for_vectorizer
import sys
sys.path.insert(0, '..')

from Utils import loadData, splitData
from tqdm import tqdm

from sklearn.feature_selection import SelectKBest, RFECV
from sklearn.feature_selection import chi2, mutual_info_classif, f_classif
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.svm import SVC, LinearSVC

import pandas as pd

def selectFeatures(selectionFunction, train_X_features, train_Y, numberOfFeaturesToSelect=10000):
    estimator = SelectKBest(selectionFunction, k=numberOfFeaturesToSelect).fit(train_X_features, train_Y)
    n_features = len(estimator.scores_)
    feature_idx_vs_score = list(zip(range(n_features), estimator.scores_))
    feature_idx_vs_score.sort(key=lambda entry: entry[1], reverse=True)
    feature_idx_vs_score = feature_idx_vs_score[:numberOfFeaturesToSelect]
    return feature_idx_vs_score

def selectFeaturesBYRecursiveFeatureSelectionWithCrossValidation(train_X_features, train_Y, estimator=LinearSVC(),
                                                                 ratio_of_features_to_drop=0.01, numberOfFeaturesToSelect=10000,
                                                                 cross_validation_steps=10):
    recursiveEstimator = RFECV(estimator=estimator, step=ratio_of_features_to_drop, cv=cross_validation_steps,
                               n_jobs=-1, verbose=1).fit(train_X_features, train_Y)
    n_features = len(recursiveEstimator.support_)
    #feature_idx_vs_support_vs_rank = list(zip(range(n_features), recursiveEstimator.support_, recursiveEstimator.ranking_))
    #feature_idx_vs_support_vs_rank_filtered = list(filter(lambda feature_entry: feature_entry[1], feature_idx_vs_support_vs_rank))
    #idx_list, _, rank_list = list(zip(*feature_idx_vs_support_vs_rank_filtered))
    #idx_vs_rank = list(zip(idx_list, rank_list))
    idx_vs_rank = list(zip(range(n_features), recursiveEstimator.ranking_))
    idx_vs_rank.sort(key=lambda feature_entry: feature_entry[1])
    idx_vs_rank = idx_vs_rank[:numberOfFeaturesToSelect]
    return idx_vs_rank

def saveFeatures(feature_names, idx_vs_score_sorted, selection_name, feature_selection_directory='feature_selection'):
    name_vs_score = list(map(lambda feature_entry: (feature_names[feature_entry[0]], feature_entry[1]), idx_vs_score_sorted))
    dataFrame = pd.DataFrame(data=name_vs_score, columns=['feature_name', 'score_or_rank'])
    feature_file_path = os.path.join(feature_selection_directory, '{}.csv'.format(selection_name))
    dataFrame.to_csv(feature_file_path, index=None, quotechar='"')

def performFeatureSelection():
    dataFilesPaths= ['../../Data/twitter-2013train.txt',
                     '../../Data/twitter-2015train.txt',
                     '../../Data/twitter-2016train.txt']
    sentences, labels = loadData(dataFilesPaths)
    train_sentences, _, train_labels, _ = splitData(sentences, labels)
    vectorizer = CountVectorizer(input=sentences, lowercase=False,
                                 preprocessor=lambda a: preprocess_sentence_ekphrasis(a),
                                 tokenizer=tokenize_sentence_ekphrasis_for_vectorizer,
                                 binary=True)
    features = vectorizer.fit_transform(raw_documents=train_sentences)
    feature_selection_functions = [chi2, mutual_info_classif, f_classif]
    for feature_selection_function in tqdm(feature_selection_functions):
        function_name = feature_selection_function.__name__
        print('selecting features according to: {}...'.format(function_name))
        selected_features = selectFeatures(feature_selection_function, features, train_labels, numberOfFeaturesToSelect=10000)
        saveFeatures(vectorizer.get_feature_names(), selected_features, function_name, feature_selection_directory='feature_selection')
    function_name = 'recursive_feature_elamination_with_cross_validation'
    print('selecting features according to: {}...'.format(function_name))
    selected_features = selectFeaturesBYRecursiveFeatureSelectionWithCrossValidation(features, train_labels, estimator=LinearSVC(),
                                                                                     cross_validation_steps=10, numberOfFeaturesToSelect=10000)
    saveFeatures(vectorizer.get_feature_names(), selected_features, function_name,
                 feature_selection_directory='feature_selection')

if __name__ == '__main__':
    performFeatureSelection()
