import sys
import re

sys.path.insert(0, '..')
from Utils import loadStopWords

from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from sklearn.base import BaseEstimator

from ekphrasis.classes.preprocessor import TextPreProcessor
from ekphrasis.classes.tokenizer import SocialTokenizer
from ekphrasis.dicts.emoticons import emoticons

default_text_processor = TextPreProcessor(
        # terms that will be normalized
        normalize=['url', 'email', 'percent', 'money', 'phone', 'user',
                   'time', 'url', 'date', 'number'],
        # terms that will be annotated
        annotate={"hashtag", "allcaps", "elongated", "repeated",
                  'emphasis', 'censored'},
        fix_html=True,  # fix HTML tokens

        # corpus from which the word statistics are going to be used
        # for word segmentation
        segmenter="twitter",

        # corpus from which the word statistics are going to be used
        # for spell correction
        corrector="twitter",

        unpack_hashtags=True,  # perform word segmentation on hashtags
        unpack_contractions=True,  # Unpack contractions (can't -> can not)
        spell_correct_elong=False,  # spell correction for elongated words

        # select a tokenizer. You can use SocialTokenizer, or pass your own
        # the tokenizer, should take as input a string and return a list of tokens
        tokenizer=SocialTokenizer(lowercase=True).tokenize,

        # list of dictionaries, for replacing tokens extracted from the text,
        # with other expressions. You can pass more than one dictionaries.
        dicts=[emoticons]
    )

def preprocess_sentence_ekphrasis(sentence, textprocessor=None):
    if textprocessor is None:
        textprocessor = default_text_processor
    return textprocessor.pre_process_doc(sentence)

# this returns the same sentenceToken as it is expected to be already tokenized using preprocess_sentence_ekphrasis
def tokenize_sentence_ekphrasis_for_vectorizer(sentenceTokens):
    return sentenceTokens

PORTER_STEMMER = PorterStemmer()
def light_preprocessor(sentence):
    tokens = re.split("\W+", sentence) # tokenizer
    tokens_after_preprocessing = []
    for token in tokens:
        if len(token) == 0:
            continue
        token = token.lower()
        #token = PORTER_STEMMER.stem(token)
        tokens_after_preprocessing.append(token)
    return tokens_after_preprocessing

NLTK_STOP_WORDS =  set(stopwords.words("english"))
DR_STOP_WORDS = loadStopWords(filePath='../../Resources/stopwords.txt')

class PreprocessorEstimator(BaseEstimator):
    def __init__(self, preprocessorOptions={}, default_preprocessor_option='',
                 feature_words_options={}, default_feature_words_option=None,
                 stop_words_options={}, default_stop_words_option={}):
        self.params_dict = {
            'preprocessorOptions':preprocessorOptions,
            'default_preprocessor_option':default_preprocessor_option,
            'feature_words_options':feature_words_options,
            'default_feature_words_option':default_feature_words_option,
            'stop_words_options':stop_words_options,
            'default_stop_words_option':default_stop_words_option
        }

    def get_params(self, deep=True):
        return  self.params_dict

    def set_params(self, **params):
        for key, val in params.items():
            self.params_dict[key] = val

    def fit(self, X, y, **fitparams):
        return self

    def transform(self, X):
        preprocessing_function = self.params_dict['preprocessorOptions'][self.params_dict['default_preprocessor_option']]
        #features = []
        feature_words = self.params_dict['feature_words_options'].get(self.params_dict['default_feature_words_option'], None) #self.params_dict['feature_words_options'][self.params_dict['default_feature_words_option']] if self.params_dict['default_feature_words_option'] is not None else None
        current_stop_words = self.params_dict['stop_words_options'].get(self.params_dict['default_feature_words_option'], None)#self.params_dict['stop_words_options'][self.params_dict['default_feature_words_option']] if self.params_dict['default_feature_words_option'] is not None else None
        features = list(map(lambda sentence: ' '.join(
                            list(filter(lambda word: feature_words is None or
                                                                      ((word in feature_words) and (current_stop_words is None or word not in current_stop_words))
                                                         , preprocessing_function(sentence)))), X))
        return features