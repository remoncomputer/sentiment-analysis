import os
import sys
import itertools

sys.path.insert(0, '..')
from Utils import loadData, loadStopWords, loadLexicons, splitData
from preprocessing import preprocess_sentence_ekphrasis, tokenize_sentence_ekphrasis_for_vectorizer, \
    light_preprocessor, NLTK_STOP_WORDS, DR_STOP_WORDS, PreprocessorEstimator

from tqdm import tqdm
import pandas as pd
import numpy as np
import time

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer # maybe I should examin other vectorizers
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV

from sklearn.svm import SVC, LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB, MultinomialNB
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression

# see https://scikit-learn.org/stable/auto_examples/classification/plot_classifier_comparison.html for classifier details

# perform grid search with diffrent classifers with diffrent attributes using piplines
# diffrent preprosessors are: mySimplePreprocessor, ekphrasis Preprocessor
# I might try using Word vectors with diffrent embeddings
# diffrent features to try are countVectorizer(With binary on and off), TFIDF
# I might use feature words from lexicons, diffrent feature selection algorithms (I might limit those to top [100, 500, 1000, 2000, 5000, 10000])
# I might try using some ngrams
# I may or may not use the stop_words_list
# classifiers I should use: SVM(Linear and RBF), NaiveBayes, LogesticRegression, see other classifiers like Perceptron
# I need to make a function that generates the pipelines with it's gridsearch
# I need to save each grid search result with approperiate pipleine name, also append the date with the name and result format

def loadFeatureSelectionWords(feature_method_names=[
                                                    'chi2',
                                                    'f_classif',
                                                    'mutual_info_classif',
                                                    'recursive_feature_elamination_with_cross_validation'
],
                              top_n_selections=[100, 500, 1000, 2000, 5000, 10000], #
                              feature_selection_directory='feature_selection'):
    feature_names_vs_word_features=[]
    for feature_method_name in feature_method_names:
        features_path = os.path.join(feature_selection_directory, '{}.csv'.format(feature_method_name))
        dataFrame = pd.read_csv(features_path, index_col=None)
        words_list_sorted = list(dataFrame['feature_name'])
        for selection_n in top_n_selections:
            feature_name ='{}_top_{}_words'.format(feature_method_name, selection_n)
            feature_words = words_list_sorted[:selection_n]
            feature_names_vs_word_features.append((feature_name, feature_words))
    return feature_names_vs_word_features

def loadWordsFeatures():
    feature_names_vs_word_features = loadFeatureSelectionWords()
    positive_lexicon_words, negative_lexicon_words = loadLexicons(positiveLexiconFilePath='../../Resources/opinion-lexicon-English-Bing_liu/positive-words.txt',
                 negativeLexiconFilePath='../../Resources/opinion-lexicon-English-Bing_liu/negative-words.txt')
    lexicon_words = positive_lexicon_words + negative_lexicon_words
    feature_names_vs_word_features.append(('lexicon_words', lexicon_words))
    return feature_names_vs_word_features

# when making
# represents a grid search experiment to be run
class GridSearchExperiment:
    def __init__(self, stages_list, cross_validation=10):
        self.stages = stages_list
        self.cross_validation = cross_validation

    def compile_pipeline(self):
        pipeline_stages = []
        grid_search_parameters_dict = {}
        self.stages_names = []
        for stage in self.stages:
            current_option = stage.options[0]
            option_name = current_option.name
            stage_name = '{}_{}'.format(stage.name, option_name)
            self.stages_names.append(stage_name)
            pipeline_stages.append((stage_name, current_option.option))
            for key, val in current_option.parameters.items():
                new_parameter_name = '{}__{}'.format(stage_name, key)
                grid_search_parameters_dict[new_parameter_name] = val
        pipline = Pipeline(pipeline_stages)
        gridSearchEstimator = GridSearchCV(estimator=pipline,
                                           param_grid=grid_search_parameters_dict,
                                           n_jobs=1, iid=True, cv=self.cross_validation,
                                           verbose=20, return_train_score=True,
                                           scoring=['accuracy', 'f1_macro'], refit=False)
        return gridSearchEstimator


    def perform_experiment(self, X, y):
        self.gridSearchEstimator = self.compile_pipeline()
        self.gridSearchEstimator.fit(X, y)
        results = self.gridSearchEstimator.cv_results_
        self.resultsDF = pd.DataFrame(results)

    def save_results(self, results_directory='results'):
        exp_time_prefix = time.strftime('%Y-%m-%d_%H:%M:%S')
        result_file_name = exp_time_prefix + '_' + '_'.join(self.stages_names) + '.csv'
        # for stage_name in self.stages_names:
        #     result_file_name += stage_name + '_'
        self.resultsDF.to_csv(path_or_buf=os.path.join(results_directory, result_file_name), index=None)

    # def save_best_model(self, models_directory='models'):
    #     pass

def GridSearchExperimentGenerator(stages_list, number_of_cross_validation=10):
    options_per_stage = [stage.options for stage in stages_list]
    for option_combination in itertools.product(*options_per_stage):
        stages_in_current_experiment = []
        for stage, option in zip(stages_list, option_combination):
            current_stage = GridSearchStage(name=stage.name, options=[option])
            stages_in_current_experiment.append(current_stage)
        current_experiment = GridSearchExperiment(stages_list=stages_in_current_experiment, cross_validation=number_of_cross_validation)
        yield current_experiment

# represents a grid search stage
class GridSearchStage:
    def __init__(self, name, options):
        self.name = name
        self.options = options


# represent an option taken by the grid stage
class GridSearchStageOption:
    def __init__(self, option, parameters):
        self.option = option
        self.name = option.__class__.__name__
        self.parameters = parameters

def preprocess_sentence_with_ekphrasis_with_default_text_preprocessor(sentence):
    return preprocess_sentence_ekphrasis(sentence)

def run_experiments():
    dataFilesPaths = ['../../Data/twitter-2013train.txt',
                      '../../Data/twitter-2015train.txt',
                      '../../Data/twitter-2016train.txt']
    corpus, labels = loadData(dataFilesPaths)
    words_vocabulary = loadWordsFeatures()
    words_vocabulary_name_vs_list = {entry[0]:entry[1] for entry in words_vocabulary}
    words_vacabulary_name = list(words_vocabulary_name_vs_list.keys())
    global_random_state = 7
    # don't forget to try without using stop_words, with dr stop_words, with nltk stop words
    stages_list = [GridSearchStage(name='preprocessor',
                                   options=[
                                       GridSearchStageOption(option=PreprocessorEstimator(
                                           preprocessorOptions={'light_preprocessor':light_preprocessor,
                                                                'ekphrasis':preprocess_sentence_with_ekphrasis_with_default_text_preprocessor},
                                           feature_words_options=words_vocabulary_name_vs_list,
                                           stop_words_options={'NLTK':NLTK_STOP_WORDS,
                                                               'PROVIDED_STOP_WORDS': DR_STOP_WORDS
                                                               }
                                       ),
                                                             parameters={'default_preprocessor_option':['light_preprocessor', 'ekphrasis'], #
                                                                         'default_feature_words_option': words_vacabulary_name,
                                                                            'default_stop_words_option':['NLTK', None, 'PROVIDED_STOP_WORDS'] #
                                                                         }
                                       )
                                           ]),
                                           GridSearchStage(name='vectorizer',
                                   options=[
                                       GridSearchStageOption
                                           (
                                           # don't forget to vary n-gram range and try diffrent n-grams
                                           option=CountVectorizer( max_features=10000), #preprocessor=lambda features: features, tokenizer=lambda tokens:tokens, stop_words=None,
                                           parameters={'binary': (False, True), # ,
                                                       'lowercase': [False]
                                                       }
                                       ),
                                       GridSearchStageOption
                                           (
                                           # vary_ngram_range and Use_IDF and rest of parameters
                                           option=TfidfVectorizer(max_features=10000), #preprocessor=lambda features: features, tokenizer=lambda tokens:tokens, stop_words=None,
                                           parameters={'use_idf':[True, False],
                                                       'smooth_idf':[True, False],
                                                       'binary':[True, False],
                                                       'sublinear_tf':[True, False],
                                                       'lowercase': [False],
                                                       'norm': ['l2', 'l1']
                                                       }
                                       )
                                   ]),
                   GridSearchStage(name='classifier',
                                   options=[
                                       GridSearchStageOption
                                           (
                                           option=LogisticRegression(n_jobs=-1, random_state=global_random_state),
                                           parameters={'C': np.logspace(start=0.01, stop=100, num=10, endpoint=True)
                                                       }
                                       ),
                                       GridSearchStageOption
                                           (
                                           option=SVC(random_state=global_random_state),
                                           parameters={'C': np.linspace(start=0.01, stop=100, num=10, endpoint=True),
                                                       # num=10
                                                       'kernel': ('linear', 'poly', 'rbf', 'sigmoid'),  #
                                                       }
                                       )
                                   ])
                   ]
    print('performing experiments ...')
    for experiment in tqdm(GridSearchExperimentGenerator(stages_list, number_of_cross_validation=10)): # number_of_cross_validation=10
        experiment.perform_experiment(X=corpus, y=labels)
        experiment.save_results(results_directory='results')
        #experiment.save_best_model()

# maybe I can make a plot like this: https://scikit-learn.org/stable/auto_examples/model_selection/plot_multi_metric_evaluation.html#sphx-glr-auto-examples-model-selection-plot-multi-metric-evaluation-py