from tensorflow.data import Dataset
from tensorflow.data.experimental import AUTOTUNE
import tensorflow as tf
from tensorflow.keras.regularizers import l2

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from dataset import loadEmbeddings, preprocess_data

def plot_graphs(history, string):
  plt.plot(history.history[string])
  plt.plot(history.history['val_'+string])
  plt.xlabel("Epochs")
  plt.ylabel(string)
  plt.legend([string, 'val_'+string])
  plt.show()

class MyModel(tf.keras.Model):
    def __init__(self, embeddingMatrix):
        super(MyModel, self).__init__()
        nb_filter = 200
        hidden_dims = nb_filter
        self.embeddingLayer = tf.keras.layers.Embedding(input_dim=embeddingMatrix.shape[0], output_dim=embeddingMatrix.shape[1],
                                        input_length=60, weights=[embeddingMatrix], trainable=False)
        self.conv1 = tf.keras.layers.Conv1D(filters=nb_filter, kernel_size=6, activation='relu', kernel_regularizer=l2(0.001))
        self.maxBool1 = tf.keras.layers.MaxPool1D(6, 2)
        self.BatchNormalization = tf.keras.layers.BatchNormalization()
        self.conv2 = tf.keras.layers.Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_regularizer=l2(0.001))
        self.maxBool2 = tf.keras.layers.MaxPool1D(6, 2)
        self.flatten = tf.keras.layers.Flatten()
        self.dense1 = tf.keras.layers.Dense(hidden_dims, activation='relu', kernel_regularizer=l2(0.001))
        self.outputLayer = tf.keras.layers.Dense(units=3, activation='softmax')

    def call(self, X, training=False):
        X = self.embeddingLayer(X)
        X = self.conv1(X)
        X = self.maxBool1(X)
        X = self.BatchNormalization(X, training)
        X = self.conv2(X)
        X = self.maxBool2(X)
        X = self.flatten(X)
        X = self.dense1(X)
        X = self.outputLayer(X)
        return X
if __name__ == '__main__':
    dataFilesPaths = ['../../Data/twitter-2013train.txt',
                  '../../Data/twitter-2015train.txt']
    train_DS = preprocess_data(dataFilesPaths)
    dataFilesPaths = ['../../Data/twitter-2016train.txt']
    valid_DS = preprocess_data(dataFilesPaths)

    wordToEmbeddingIdx, embeddingMatrix = loadEmbeddings('embeddings/datastories.twitter.50d.txt')

    nb_filter = 5
    hidden_dims = nb_filter
    # model = tf.keras.Sequential()
    #
    # model.add()
    # model.add(tf.keras.layers.Conv1D(filters=nb_filter, kernel_size=6, activation='relu', kernel_regularizer=l2(0.001)))
    # model.add(tf.keras.layers.MaxPool1D(6, 2))
    #
    # model.add(tf.keras.layers.BatchNormalization())
    # model.add(tf.keras.layers.Conv1D(filters=nb_filter, kernel_size=3, activation='relu', kernel_regularizer=l2(0.001)))
    # model.add(tf.keras.layers.MaxPool1D(6, 2))
    #
    # model.add(tf.keras.layers.Flatten())
    # model.add(tf.keras.layers.Dense(hidden_dims, activation='relu', kernel_regularizer=l2(0.001)))
    #
    # model.add(tf.keras.layers.Dense(units=3, activation='softmax'))
    model = MyModel(embeddingMatrix)

    model.compile(loss=tf.keras.losses.SparseCategoricalCrossentropy(),
                  optimizer='adam',
                  metrics=[tf.metrics.SparseCategoricalAccuracy()])
    #model.summary()
    history = model.fit(train_DS, shuffle=True, epochs=50, validation_data=valid_DS)

    model.save('models/m.h5')
