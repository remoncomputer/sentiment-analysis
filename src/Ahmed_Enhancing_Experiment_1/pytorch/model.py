import torch
import torch.nn as nn
import torch.nn.functional as F

class M1(nn.Module):
    def __init__(self, embeddingMatrix, n_lstm_layers, lstm_layer_size, dropout_propability, bidirectional, sequence_length=50):
        super(M1, self).__init__()
        number_of_tokens_in_embedding = embeddingMatrix.shape[0]
        number_of_features_per_token = embeddingMatrix.shape[1]
        # make the embedding_layer here
        self.embeding = nn.Embedding(num_embeddings=number_of_tokens_in_embedding,
                                     embedding_dim=number_of_features_per_token,
                                     _weight=embeddingMatrix)
        self.n_lstm_layer = n_lstm_layers
        self.lstm = nn.LSTM(input_size=number_of_features_per_token,
                            hidden_size=lstm_layer_size, num_layers=n_lstm_layers,
                            batch_first=True, dropout=dropout_propability,
                            bidirectional=bidirectional)# Be ware that the last layer doesn't contain dropout, don't know if batch_first should be true or false
        self.size_input_for_output = sequence_length * lstm_layer_size * (1 + int(bidirectional==True))
        self.outputLayer = nn.Linear(in_features=self.size_input_for_output, out_features=3)

    def forward(self, X):
        X = self.embeding(X)
        X, _ = self.lstm(X) # require to see dims for debugging
        #X = X.permute(1, 0, 2)
        X = X.contiguous().view(-1, self.size_input_for_output)
        X = self.outputLayer(X)
        return X

class CNN2d(nn.Module):
    def __init__(self, embeddingMatrix, n_filters, filter_sizes, output_dim, dropout_propability):
        
        super().__init__()
        number_of_tokens_in_embedding = embeddingMatrix.shape[0]
        number_of_features_per_token = embeddingMatrix.shape[1]
        
        self.embedding = nn.Embedding(num_embeddings=number_of_tokens_in_embedding,
                                     embedding_dim=number_of_features_per_token,
                                     _weight=embeddingMatrix)
        self.embedding.weight.requires_grad = False
        
        self.convs = nn.ModuleList([nn.Conv2d(in_channels = 1, 
                                              out_channels = n_filters, 
                                              kernel_size = (fs, number_of_features_per_token)) 
                                    for fs in filter_sizes
                                    ])
        
        self.fc = nn.Linear(len(filter_sizes) * n_filters, output_dim)
        
        self.dropout = nn.Dropout(dropout_propability)
        
    def forward(self, text):
        
        #text = [sent len, batch size]
        
        text = text.permute(1, 0)
                
        #text = [batch size, sent len]
        
        embedded = self.embedding(text)
                
        #embedded = [batch size, sent len, emb dim]
        
        embedded = embedded.unsqueeze(1)
        
        #embedded = [batch size, 1, sent len, emb dim]
        
        conved = [F.relu(conv(embedded)).squeeze(3) for conv in self.convs]
            
        #conved_n = [batch size, n_filters, sent len - filter_sizes[n] + 1]
                
        pooled = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conved]
        
        #pooled_n = [batch size, n_filters]
        
        cat = self.dropout(torch.cat(pooled, dim = 1))

        #cat = [batch size, n_filters * len(filter_sizes)]
        y_pred =  self.fc(cat)
        print(np.shape(y_pred))
        return y_pred
        
        
class CNN1d(nn.Module):
    def __init__(self, embeddingMatrix, n_filters, filter_sizes, output_dim, dropout_propability, sequence_length=50):
        
        super().__init__()
        number_of_tokens_in_embedding = embeddingMatrix.shape[0]
        number_of_features_per_token = embeddingMatrix.shape[1]
        
        self.embedding = nn.Embedding(num_embeddings=number_of_tokens_in_embedding,
                                     embedding_dim=number_of_features_per_token,
                                     _weight=embeddingMatrix)
                                     
        self.convs = nn.ModuleList([
                                    nn.Conv2d(in_channels = number_of_features_per_token, 
                                              out_channels = n_filters, 
                                        kernel_size = fs)
                                    for fs in filter_sizes
                                    ])
        
        self.fc = nn.Linear(len(filter_sizes) * n_filters, output_dim)
        
        self.dropout = nn.Dropout(dropout_propability)
        self.outputLayer = nn.Linear(in_features=self.size_input_for_output, out_features=3)
    def forward(self, text):
        

        embedded = self.embedding(text)

        conved = [F.relu(conv(embedded)).squeeze(3) for conv in self.convs]
            
        pooled = [F.max_pool1d(conv, conv.shape[2]).squeeze(2) for conv in conved]
        
        cat = self.dropout(torch.cat(pooled, dim = 1))

        fc1 = self.fc(cat)
        
        return self.outputLayer(fc1)