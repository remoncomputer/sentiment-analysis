import pickle
from tqdm import tqdm
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow.data import Dataset
from tensorflow.data.experimental import AUTOTUNE
import sys
sys.path.insert(0, '..')
from Utils import loadData
from preprocessData import getTextProcessorWithConfiguration1

def padSentenceAndConvertToEmbeddings(sentence, textProcessor, maxSequenceLength, padIndex, unkIndex, wordToEmbeddingIndex):
    preprocessedTokensTrimmed = textProcessor.pre_process_doc(sentence)[:maxSequenceLength]
    featureIndices = [wordToEmbeddingIndex.get(word, unkIndex) for word in preprocessedTokensTrimmed]
    remainingLength = maxSequenceLength - len(preprocessedTokensTrimmed)
    featureIndices = featureIndices + [padIndex] * remainingLength
    return featureIndices
    
    #remainingLength = maxSequenceLength
def MyDataset(path, filePaths, maxSequenceLength, textProcessor, wordToEmbeddingIndex, batchSize):
    sentences, labels = loadData(filePaths)
    unkIndex = wordToEmbeddingIndex['<unk>']
    padIndex = wordToEmbeddingIndex['<pad>']
    features = list(map(lambda sentence: padSentenceAndConvertToEmbeddings(sentence, textProcessor, maxSequenceLength, padIndex, unkIndex, wordToEmbeddingIndex), sentences))
    if path == None: 
        labelsDS = Dataset.from_tensor_slices(labels)
        labelsDS = labelsDS.map(lambda label: 0 * tf.cast(tf.equal(label,'negative'), tf.int32) + 1 * tf.cast(tf.equal(label,'neutral'), tf.int32) + 2 * tf.cast(tf.equal(label, 'positive'), tf.int32), num_parallel_calls=AUTOTUNE)
        featuresDS = Dataset.from_tensor_slices(features)
        Data = Dataset.zip((featuresDS, labelsDS)).shuffle(buffer_size=len(features), seed=7, reshuffle_each_iteration=True).batch(batchSize)
        return Data
    else:
        with  open(path, 'wb') as pkl_file:
            data = list(zip(features, labels))
            pickle.dump(data, pkl_file)

def loadEmbeddings(embeddingsFilePath):
    # dataFrame = pd.read_csv(embeddingsFilePath, sep=' ', header=None, index_col=0)
    # words = list(dataFrame.index)
    # wordIndices = np.arange(len(words))
    # wordToEmbeddingIdx = {word: idx for idx, word in zip(wordIndices, words)}
    # embeddingMatrix = dataFrame.as_matrix()
    word_counter = 0
    wordToEmbeddingIdx = {}
    embeddingMatrix = []
    with open(embeddingsFilePath, mode='r') as f:
        #lines = f.readlines()
        for line in f:
            tokens = line.split(' ')
            word = tokens[0]
            features = [float(num) for num in tokens[1:]]
            embeddingMatrix.append(features)
            wordToEmbeddingIdx[word] = word_counter
            word_counter += 1
    embeddingMatrix = np.array(embeddingMatrix, dtype=np.float)
    return wordToEmbeddingIdx, embeddingMatrix

def preprocess_data(dataFilesPaths, path = None, maxSequenceLength = 60, batchSize = 64):
    embeddingsFilePath = 'embeddings/datastories.twitter.50d.txt'
    wordToEmbeddingIdx, embeddingMatrix = loadEmbeddings(embeddingsFilePath)

    textPreprocessor = getTextProcessorWithConfiguration1()
    return MyDataset(path, dataFilesPaths, maxSequenceLength, textPreprocessor, wordToEmbeddingIdx, batchSize)