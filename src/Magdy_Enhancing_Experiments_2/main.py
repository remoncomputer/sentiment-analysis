import sys
sys.path.insert(0, '..')
from Utils import loadData

import random

from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_validate, cross_val_score


import re
import pickle
import yaml
import os


from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
from nltk.stem.lancaster import LancasterStemmer
from nltk.stem import WordNetLemmatizer as wnl
import numpy as np


def removeEmptyTokens(tokens):
    return [x for x in tokens if x != '']
def tokenizeCorpus(corpus):
    tokenized_sentences = []
    for sentence in corpus:
        sentence = sentence.strip()
        tokens = re.split("\W+", sentence)
        tokens = removeEmptyTokens(tokens)
        tokenized_sentences.append(tokens)
    return tokenized_sentences

def caseFolding(tokenized_corpus):
    corpus_after_case_folding = []
    for tokenized_sentence in tokenized_corpus:
        tokenized_sentence_after_case_folding = []
        for word in tokenized_sentence:
            new_word = word.lower() # Check this function
            tokenized_sentence_after_case_folding.append(new_word)
        corpus_after_case_folding.append(tokenized_sentence_after_case_folding)
    return corpus_after_case_folding

STOP_WORDS = set(stopwords.words("english"))
def removeStopWords(tokenized_corpus):
    corpus_after_removing_stop_words = []
    for tokenized_sentence in tokenized_corpus:
        tokinzed_sentence_after_removing_stop_words = []
        for word in tokenized_sentence:
            if word not in STOP_WORDS:
                tokinzed_sentence_after_removing_stop_words.append(word)
        corpus_after_removing_stop_words.append(tokinzed_sentence_after_removing_stop_words)
    return corpus_after_removing_stop_words

STEMMER = PorterStemmer()
def stemCorpus(tokenized_corpus):
    corpus_after_stemming = []
    for tokenized_sentence in tokenized_corpus:
        tokenized_sentence_after_stemming = []
        for word in tokenized_sentence:
            stemmed_word = STEMMER.stem(word)
            tokenized_sentence_after_stemming.append(stemmed_word)
        corpus_after_stemming.append(tokenized_sentence_after_stemming)
    return corpus_after_stemming

def lemmaCorpus(tokenized_corpus):
    corpus_after_lemmaing = []
    for tokenized_sentence in tokenized_corpus:
        tokenized_sentence_after_lemmaing = []
        for word in tokenized_sentence:
            lemmaed_word = wnl.lemmatize(wnl,word=word,pos="v")
            #print(lemmaed_word)
            tokenized_sentence_after_lemmaing.append(lemmaed_word)
        corpus_after_lemmaing.append(tokenized_sentence_after_lemmaing)
    return corpus_after_lemmaing

def convertTokenizedCorpusToSentences(tokenized_corpus):
    sentences = []
    for tokenized_sentence in tokenized_corpus:
        sentence = ' '.join(tokenized_sentence)
        sentences.append(sentence)
    return  sentences

def preprocessCorpus(corpus):
    tokenized_corpus = tokenizeCorpus(corpus)
    tokenized_corpus = caseFolding(tokenized_corpus)
    tokenized_corpus = removeStopWords(tokenized_corpus)
    tokenized_corpus = stemCorpus(tokenized_corpus)
    #tokenized_corpus = lemmaCorpus(tokenized_corpus)
    corpus = convertTokenizedCorpusToSentences(tokenized_corpus)
    return corpus

def performExperiment(label, featureExtractor, classifier, X_train, Y_train, cross_validation_count, models_directory='results'):
    #features_train = featureExtractor.fit_transform(X_train).toarray()
    # features_test = featureExtractor.transform(X_test).toarray()
    # classifier.fit(features_train, Y_train)
    # Y_Predicted = classifier.predict(features_test)
    # accuracy = accuracy_score(Y_test, Y_Predicted)
    # f1score = f1_score(Y_test, Y_Predicted, average='macro')
    # print('Experiment {} accuracy: {} f1-score: {}'.format(label, accuracy, f1score))
    pipleine = Pipeline([('feature_extractor', featureExtractor), ('classifier', classifier)]) #

    scores_dict = cross_validate(pipleine, X=X_train, y=Y_train,
                             cv=cross_validation_count, n_jobs=-1, verbose=1,
                             scoring=['f1_macro','accuracy'], return_train_score=True) #
    #pipleine.fit(X_train, Y_train)
    #y_predict = pipleine.predict(X_train)
    print('--------------------------------------------------------------------------------------------------------')
    print('Experiment {}:'.format(label))
    print('----------------------------')
    #print(accuracy_score(y_pred=y_predict, y_true=Y_train))
    #print('scores_dict: {}'.format(scores_dict))
    results = {}
    metrics = ['train_accuracy','test_accuracy', 'train_f1_macro', 'test_f1_macro']
    for metric_name in metrics:
        metric_scores = scores_dict[metric_name]
        result_string = '{:.2f} (+/- {:.2f})'.format(metric_scores.mean(), metric_scores.std() * 2)
        results[metric_name]=result_string
        print('{}: {}'.format(metric_name, result_string))
    results_file_path = os.path.join(models_directory, '{}_results.yaml'.format(label))
    with open(results_file_path, mode='w') as f:
        yaml.dump(results, f, default_flow_style=False)
    print('results has been saved to {}'.format(results_file_path))

if __name__ == '__main__':
    dataFilesPaths = ['../../Data/twitter-2013train.txt',
                      '../../Data/twitter-2015train.txt',
                      '../../Data/twitter-2016train.txt']
    corpus, labels = loadData(dataFilesPaths)
    #X_train, X_test, Y_train, Y_test = train_test_split(corpus, labels, test_size=0.2, random_state=0)
    # Performing the experiments
    seed = 7
    random.seed(seed)
    corpus_label_list = list(zip(corpus, labels))
    random.shuffle(corpus_label_list)
    corpus, labels = list(zip(*corpus_label_list))
    labels = list(map(lambda l: 1 * int(l=='negative') + 2 * int(l=='neutral') + 3 * int(l=='positive'), labels))
    corpus = preprocessCorpus(corpus)
    
    cross_validation_count = 10
    performExperiment('CountVectorizer + Naïve Bayes Multinomial', CountVectorizer(), MultinomialNB(), corpus, labels, cross_validation_count)
    performExperiment('TFIDFVectorizer + Naïve Bayes Multinomial', TfidfVectorizer(), MultinomialNB(), corpus, labels, cross_validation_count)
    performExperiment('CountVectorizer with uni-grams and bi-grams + Naïve Bayes Multinomial',
                      CountVectorizer(ngram_range=(1, 2)), MultinomialNB(), corpus, labels, cross_validation_count)
    performExperiment('TFIDFVectorizer with uni-grams and bi-grams + Naïve Bayes Multinomial',
                      TfidfVectorizer(ngram_range=(1, 2)), MultinomialNB(), corpus, labels, cross_validation_count)
    performExperiment('CountVectorizer + Logistic Regression', CountVectorizer(), LogisticRegression(), corpus, labels, cross_validation_count)
    performExperiment('TFIDFVectorizer + Logistic Regression', TfidfVectorizer(), LogisticRegression(), corpus, labels, cross_validation_count)
    performExperiment('CountVectorizer with uni-grams and bi-grams + Logistic Regression',
                      CountVectorizer(ngram_range=(1, 2)), LogisticRegression(), corpus, labels, cross_validation_count)
    performExperiment('TFIDFVectorizer with uni-grams and bi-grams + Logistic Regression',
                      TfidfVectorizer(ngram_range=(1, 2)), LogisticRegression(), corpus, labels, cross_validation_count)