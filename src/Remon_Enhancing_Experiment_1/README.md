# First Enhancing Experiment By Remon

## Used Papers:
- [DataStories at SemEval-2017 Task 4 Deep LSTM with Attention](Papers/DataStories at SemEval-2017 Task 4 Deep LSTM with Attention.pdf)
- [Reasoning about Entailment with Neural Attention](Papers/Reasoning about Entailment with Neural Attention.pdf)
- [Feed-Forward Networks with Attention Can Solve Some Long-Term Memory Problems](Papers/Feed-Forward Networks with Attention Can Solve Some Long-Term Memory Problems.pdf)

## Notes:
- I followed [DataStories at SemEval-2017 Task 4 Deep LSTM with Attention](Papers/DataStories at SemEval-2017 Task 4 Deep LSTM with Attention.pdf) in it's methodology and used it's pre-processing tool which is [https://github.com/cbaziotis/ekphrasis](https://github.com/cbaziotis/ekphrasis)
-- See https://github.com/cbaziotis/ekphrasis/issues/11 to download the word statistics file
- I used word emdeddings that was made by the auther of the code of the paper [https://github.com/cbaziotis/datastories-semeval2017-task4](https://github.com/cbaziotis/datastories-semeval2017-task4)
- You should download the respective word embeddings and put them at the embeddings/ folder
-- [50-d twittter word embeddings] (https://mega.nz/#!zsQXmZYI!M_y65hkHdY88iC3I8Yeo7N9IRBI4D9mrpz016fqiXwQ)
-- [100-d twittter word embeddings] (https://mega.nz/#!OsYTjIrQ!gLp6YLa0A3ncXjaUffbgL2RtUI74bvSkUKpflAS0OyQ)
-- [200-d twittter word embeddings] (https://mega.nz/#!W5BXBISB!Vu19nme_shT3RjVL4Pplu8PuyaRH5M5WaNwTYK4Rxes)
-- [300-d twittter word embeddings] (https://mega.nz/#!u4hFAJpK!UeZ5ERYod-SwrekW-qsPSsl-GYwLFQkh06lPTR7K93I)
