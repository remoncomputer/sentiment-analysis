from ekphrasis.classes.preprocessor import TextPreProcessor
from ekphrasis.classes.tokenizer import SocialTokenizer
from ekphrasis.dicts.emoticons import emoticons

import pickle
from tqdm import tqdm
import sys
sys.path.insert(0, '..')
from Utils import loadData

def getTextProcessorWithConfiguration1():
    text_processor = TextPreProcessor(
        # terms that will be normalized
        normalize=['url', 'email', 'percent', 'money', 'phone', 'user',
                   'time', 'url', 'date', 'number'],
        # terms that will be annotated
        annotate={"hashtag", "allcaps", "elongated", "repeated",
                  'emphasis', 'censored'},
        fix_html=True,  # fix HTML tokens

        # corpus from which the word statistics are going to be used
        # for word segmentation
        segmenter="twitter",

        # corpus from which the word statistics are going to be used
        # for spell correction
        corrector="twitter",

        unpack_hashtags=True,  # perform word segmentation on hashtags
        unpack_contractions=True,  # Unpack contractions (can't -> can not)
        spell_correct_elong=False,  # spell correction for elongated words

        # select a tokenizer. You can use SocialTokenizer, or pass your own
        # the tokenizer, should take as input a string and return a list of tokens
        tokenizer=SocialTokenizer(lowercase=True).tokenize,

        # list of dictionaries, for replacing tokens extracted from the text,
        # with other expressions. You can pass more than one dictionaries.
        dicts=[emoticons]
    )
    return text_processor

def preprocessSentences(text_processor, sentences):
    preprocessed_sentences = []
    for sentence in tqdm(sentences):
        preprocessed_sentences.append(text_processor.pre_process_doc(sentence))
    return preprocessed_sentences

def preprocessAndSaveDataWithConfiguration1():
    print('Loading training data...')
    train_sentences, train_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013train-A.txt',
                        '../../Data/train-dev-test/twitter-2015train-A.txt',
                        '../../Data/train-dev-test/twitter-2016train-A.txt'])
    text_processor = getTextProcessorWithConfiguration1()
    print('Processing training data...')
    train_preprocessed_sentences = preprocessSentences(text_processor, train_sentences)
    print('saving train data...')
    with  open('preprocessed_data/train_data_preprocessed_configuration_1.pkl', 'wb') as pkl_file:
        data = list(zip(train_preprocessed_sentences, train_labels))
        pickle.dump(data, pkl_file)

    print('Loading validation data...')
    val_sentences, val_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'])
    print('Processing validation data...')
    val_preprocessed_sentences = preprocessSentences(text_processor, val_sentences)
    print('saving val data...')
    with  open('preprocessed_data/val_data_preprocessed_configuration_1.pkl', 'wb') as pkl_file:
        data = list(zip(val_preprocessed_sentences, val_labels))
        pickle.dump(data, pkl_file)

    print('Loading test data...')
    test_sentences, test_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013test-A.txt',
                        '../../Data/train-dev-test/twitter-2015test-A.txt',
                        '../../Data/train-dev-test/twitter-2016test-A.txt'])
    print('Processing test data...')
    test_preprocessed_sentences = preprocessSentences(text_processor, test_sentences)
    print('saving test data...')
    with  open('preprocessed_data/test_data_preprocessed_configuration_1.pkl', 'wb') as pkl_file:
        data = list(zip(test_preprocessed_sentences, test_labels))
        pickle.dump(data, pkl_file)
