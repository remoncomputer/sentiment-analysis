from dataset import loadEmbeddings, MyDataset
from preprocessData import getTextProcessorWithConfiguration1

import copy
import time
import json
import yaml
import numpy as np
import os
from tqdm import tqdm
from skopt.utils import use_named_args

import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers

def createEmbeddingLayer(embeddingMatrix, embeddingTrainable):
	#embeddingTensor = tf.convert_to_tensor(embeddingMatrix)
	embedding_layer = layers.Embedding(input_dim=embeddingMatrix.shape[0], output_dim=embeddingMatrix.shape[1],
							 weights=[embeddingMatrix], trainable=embeddingTrainable)
	return embedding_layer
	
class AttentionLayer(tf.keras.Model):
	def __init__(self, nLayers, nNeurons, activation_function, regularizer): #inputFeatureSize, #do I need this
		#super(AttentionLayer, self).__init__(trainable=True, name='MyAttentionLayer', dtype=tf.float32, dynamic=False)
		super(AttentionLayer, self).__init__(name='AttentionModel')
		self.model = tf.keras.Sequential()
		#self.model.add(layers.InputLayer(input_shape=(None, 3)))
		for _ in range(nLayers - 1):
			layer_i = layers.Dense(nNeurons, activation=activation_function, kernel_regularizer=regularizer)
			self.model.add(layer_i)
		self.model.add(layers.Dense(1, activation=activation_function, kernel_regularizer=regularizer)) #, input_shape=(300,)
		self.model.add(tf.keras.layers.Softmax()) # need to verify this numerically
		# self.layers = [layers.Dense(1, activation=activation_function, kernel_regularizer=regularizer),
		# 			   tf.keras.layers.Softmax()]

	def get_config(self):
		self.model.get_config()

	def call(self, inputs):
		features = self.model(inputs)
		# features = inputs
		# for l in self.layers:
		# 	features = l(features)
		outputs = tf.math.multiply(inputs, features) # need to verify this numerically
		outputs = tf.reduce_sum(outputs, axis=1, name='sum_accross_sequence_dim')
		return outputs

	def compute_output_shape(self, input_shape):
		shape = tf.TensorShape(input_shape).as_list()
		##shape[-1] = self.num_classes
		shape = [shape[0], shape[2]]
		return tf.TensorShape(shape)

class FScoreMetric(tf.keras.metrics.Metric):
	def __init__(self, **kwargs):
		super(FScoreMetric, self).__init__(name='f_score', **kwargs)
		self.r = tf.keras.metrics.Recall()
		self.p = tf.keras.metrics.Precision()

	def reset_states(self):
		self.r.reset_states()
		self.p.reset_states()

	def update_state(self, y_true, y_pred, sample_weight=None):
		self.r.update_state(y_true, y_pred, sample_weight)
		y_true_shape=tf.shape(y_true)
		y_pred_reshaped = tf.reshape(y_pred, shape=(y_true_shape[0], y_true_shape[1], -1))
		y_pred_labels = tf.argmax(y_pred_reshaped, axis=-1)
		y_pred_labels_squeezed = tf.squeeze(y_pred_labels, axis=-1)
		#print(y_true.numpy())
		#print(y_pred.numpy())
		self.p.update_state(y_true, y_pred_labels_squeezed, sample_weight)
		pass

	def result(self):
		p = self.p.result() # Do I need to convert to numpy
		#r = self.r.result() # Do I need to convert to numpy
		#score = 2 * p * r / (p + r)
		return p

from tensorflow.keras import backend as K

def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        """Recall metric.

        Only computes a batch-wise average of recall.

        Computes the recall, a metric for multi-label classification of
        how many relevant items are selected.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        """Precision metric.

        Only computes a batch-wise average of precision.

        Computes the precision, a metric for multi-label classification of
        how many selected items are relevant.
        """
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision
    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

class AccuracyAndFScoreWeightedMetric(tf.keras.metrics.Metric):
	def __init__(self, alpha, **kwargs):
		super(AccuracyAndFScoreWeightedMetric, self).__init__(name='accuracy_and_f_score_weighted', **kwargs)
		self.accuracy = tf.keras.metrics.Accuracy()
		self.fscore = FScoreMetric()
		self.alpha = alpha

	def reset_states(self):
		self.accuracy.reset_states()
		self.fscore.reset_states()

	def update_state(self, *args, **kwargs):
		self.accuracy.update_state(*args, **kwargs)
		self.fscore.update_state(*args, **kwargs)

	def result(self):
		score = self.alpha * self.accuracy.result() + (1 - self.alpha) * self.fscore.result()
		return score

def createModelRecurrentLayer(RNNCellType, bidirectional,
							  layersCellSize, recurrentConnectionDropout,
							  layerOutputDropout, regularizerInstance, returnSequence):
	if RNNCellType == 'LSTM':
		RNNCellInstance = tf.keras.layers.LSTM(units=layersCellSize, return_sequences=returnSequence,
											   recurrent_dropout=recurrentConnectionDropout,
											   dropout=layerOutputDropout, kernel_regularizer=regularizerInstance,
											   recurrent_regularizer=regularizerInstance)
	elif RNNCellType == 'GRU':
		RNNCellInstance = tf.keras.layers.GRU(units=layersCellSize, return_sequences=returnSequence,
											  recurrent_dropout=recurrentConnectionDropout,
											  dropout=layerOutputDropout, kernel_regularizer=regularizerInstance,
											  recurrent_regularizer=regularizerInstance)
	else:
		raise ValueError('Unknown cell Type: {}'.format(RNNCellType))
	# preparing the layer type
	if bidirectional:
		layerInstance = tf.keras.layers.Bidirectional(RNNCellInstance,
													  merge_mode='concat')  # will using copy.deepcopy introduces a problem and I need a function like tf.keras.models.clone_model
	else:
		layerInstance = RNNCellInstance
	return layerInstance

def createModel(experiment_start_time, embeddingFile, embeddingMatrix, embeddingTrainable, embeddingNoiseStandardDeviation, embeddingDropout,
				bidirectional, nLayers, layersCellSize, RNNCellType, recurrentConnectionDropout,
				layerOutputDropout, attentionNumberOfLayers, attentionNumberOfNeuronsPerLayer,
				attentionLayerActivationFunction, l2RegularizationPenalityWeight, optimizerLearningRate,
				optimizerType, AccuracyFScoreAlpha, otherMetricsToDisplay, lossesWeightsPerClass, clipNorm):

	config_dict={'embeddingFile':embeddingFile,
				 'embeddingTrainable':embeddingTrainable,
				 'embeddingNoiseStandardDeviation':embeddingNoiseStandardDeviation,
				 'embeddingDropout':embeddingDropout, 'bidirectional':bidirectional,
				 'nLayers':nLayers, 'layersCellSize':layersCellSize, 'RNNCellType':RNNCellType,
				 'recurrentConnectionDropout':recurrentConnectionDropout, 'layerOutputDropout':layerOutputDropout,
				 'attentionNumberOfLayers':attentionNumberOfLayers,
				 'attentionNumberOfNeuronsPerLayer':attentionNumberOfNeuronsPerLayer,
				 'attentionLayerActivationFunction':attentionLayerActivationFunction,
				 'l2RegularizationPenalityWeight':l2RegularizationPenalityWeight,
				 'optimizerLearningRate':optimizerLearningRate,
				 'optimizerType':optimizerType, 'AccuracyFScoreAlpha':AccuracyFScoreAlpha,
				 'otherMetricsToDisplay':otherMetricsToDisplay,
				 'lossesWeightsPerClass':str(list(lossesWeightsPerClass.numpy())),
				 'clipNorm':clipNorm
				 }
	with open('models/{}_config.yaml'.format(experiment_start_time), 'w') as config_file:
		yaml.dump(config_dict, config_file, default_flow_style=False)
	regularizerInstance = tf.keras.regularizers.l2(l=l2RegularizationPenalityWeight)
	# creating the model
	model = tf.keras.Sequential()
	# creating and adding the embedding Layer
	embeddingLayer = createEmbeddingLayer(embeddingMatrix, embeddingTrainable)
	model.add(embeddingLayer)
	# creating and adding the noise layer
	gaussianNoiseLayer = tf.keras.layers.GaussianNoise(stddev=embeddingNoiseStandardDeviation)
	model.add(gaussianNoiseLayer)
	# creating and adding dropout
	embeddingDropout = tf.keras.layers.Dropout(rate=embeddingDropout)
	model.add(embeddingDropout)
	# preparing the recurrent layers
	# preparing the cell type
	# if RNNCellType == 'LSTM':
	# 	RNNCellInstance = tf.keras.layers.LSTM(units=layersCellSize, return_sequences=True,
	# 										   recurrent_dropout=recurrentConnectionDropout,
	# 										   dropout=layerOutputDropout, kernel_regularizer=regularizerInstance,
	# 										   recurrent_regularizer=regularizerInstance)
	# elif RNNCellType == 'GRU':
	# 	RNNCellInstance = tf.keras.layers.GRU(units=layersCellSize, return_sequences=True,
	# 										  recurrent_dropout=recurrentConnectionDropout,
	# 										  dropout=layerOutputDropout, kernel_regularizer=regularizerInstance,
	# 										  recurrent_regularizer=regularizerInstance)
	# else:
	# 	raise ValueError('Unknown cell Type: {}'.format(RNNCellType))
	# # preparing the layer type
	# if bidirectional:
	# 	layerInstance = tf.keras.layers.Bidirectional(RNNCellInstance, merge_mode='concat') # will using copy.deepcopy introduces a problem and I need a function like tf.keras.models.clone_model
	# else:
	# 	layerInstance = RNNCellInstance
	# # adding the recurrent layers
	# for layer_idx in range(nLayers):
	# 	layer_copy = copy.deepcopy(layerInstance)
	# 	layer_copy.name = 'layer_{}'.format(layer_idx)
	# 	model.add(layer_copy)
	for _ in range(nLayers):
		model.add(createModelRecurrentLayer(RNNCellType, bidirectional,
							  layersCellSize, recurrentConnectionDropout,
							  layerOutputDropout, regularizerInstance, True))
	# creating and adding the attention layer
	attentionLayer = AttentionLayer(nLayers=attentionNumberOfLayers, nNeurons=attentionNumberOfNeuronsPerLayer,
									activation_function=attentionLayerActivationFunction, regularizer=regularizerInstance)
	model.add(attentionLayer)
	# model.add(tf.keras.layers.LSTM(units=layersCellSize, return_sequences=False,
	# 					 recurrent_dropout=recurrentConnectionDropout,
	# 					 dropout=layerOutputDropout, kernel_regularizer=regularizerInstance,
	# 					 recurrent_regularizer=regularizerInstance))
	# model.add(createModelRecurrentLayer(RNNCellType, bidirectional,
	# 									layersCellSize, recurrentConnectionDropout,
	# 									layerOutputDropout, regularizerInstance, False))

	# maxoutLayer - dh thyees
	#keras.layers.core.MaxoutDense
	outputLayer = tf.keras.layers.Dense(units=3, activation='linear', kernel_regularizer=regularizerInstance)
	model.add(outputLayer)

	#making the optimizer
	if optimizerType == 'Adam':
		optimizer = tf.optimizers.Adam(learning_rate=optimizerLearningRate, clipnorm=clipNorm)
	elif optimizerType == 'RMSprop':
		optimizer = tf.optimizers.RMSprop(learning_rate=optimizerLearningRate, clipnorm=clipNorm)
	elif optimizerType == 'SGD':
		optimizer = tf.optimizers.SGD(learning_rate=optimizerLearningRate, clipnorm=clipNorm)
	else:
		raise ValueError('Unsupported optimizer: {}'.format(optimizerType))


	metrics = ['sparse_categorical_accuracy'] #,f1 , FScoreMetric() , AccuracyAndFScoreWeightedMetric(alpha=AccuracyFScoreAlpha)
	metrics.extend(otherMetricsToDisplay)
	model.summary()
	model.compile(optimizer=optimizer,
				  loss='sparse_categorical_crossentropy',
				  metrics=metrics) #, loss_weights=list(lossesWeightsPerClass.numpy())
	return model

# def create_model_conf_1(........): # don't forget noise, various dropouts, regularization
# 	pass
#
# def getWieghtedMetric(alpha):
# 	pass
	
def trainModel(experiment_start_time, model, train_data, val_data,
			   lossesWeightsPerClass, MAXEPOCHS=100, logs_folder_path='logs', models_folder_path='model',
			   early_stopping_delta=0.001, early_stopping_patience=10):
	#trainInputs = train_data.map(lambda a,b: a)
	#trainLabels = train_data.map(lambda a,b: b)
	callbacks = [
		tf.keras.callbacks.TensorBoard(log_dir='{}/exp_{}'.format(logs_folder_path, experiment_start_time),
									   update_freq='epoch'), #exp_{}
				 tf.keras.callbacks.ModelCheckpoint(filepath='{}/{}_best_model.pth'.format(models_folder_path, experiment_start_time),
													monitor='val_loss', #'val_sparse_categorical_accuracy'  'val_accuracy_and_f_score_weighted'
													verbose=1, save_best_only=True,
													mode='min', save_weights_only=False),
				 tf.keras.callbacks.EarlyStopping(monitor='val_loss', #'val_sparse_categorical_accuracy''val_accuracy_and_f_score_weighted'
												  min_delta=early_stopping_delta,
												  patience=early_stopping_patience,
												  verbose=1,
												  mode='min',
												  restore_best_weights=False)
		]
	# model.save(filepath='{}/{}_best_model.pth'.format(models_folder_path, experiment_start_time),
	# 		   overwrite=True,
	# 		   include_optimizer=True)
	history=model.fit(x=train_data,
					  class_weight=list(lossesWeightsPerClass.numpy()),
					  shuffle=True, workers=8,
					  use_multiprocessing=True, validation_data=val_data,
					  epochs=MAXEPOCHS, verbose=2, callbacks=callbacks)
	metric_history_name = 'val_loss' # 'val_sparse_categorical_accuracy'
	history_metric_values = history.history[metric_history_name]
	return np.max(history_metric_values)
	
class FitnessObject:
	def __init__(self,
				 train_data_files=['../Data/train-dev-test/twitter-2013train-A.txt',
                             '../Data/train-dev-test/twitter-2015train-A.txt',
                             '../Data/train-dev-test/twitter-2016train-A.txt'],
				 val_data_files=['../Data/train-dev-test/twitter-2013val-A.txt',
                             '../Data/train-dev-test/twitter-2015val-A.txt',
                             '../Data/train-dev-test/twitter-2016val-A.txt'],
				 maxSequenceLength=50, batchSize=64, textProcessorInstance=getTextProcessorWithConfiguration1(),
				 MAXEPOCHS=100, logs_folder_path='logs', models_folder_path='models',
				 early_stopping_delta=0.001, early_stopping_patience=10,
				 AccuracyFScoreAlpha = 0.5,
				 embeddings_file_paths=['embeddings/datastories.twitter.50d.txt',
										'embeddings/datastories.twitter.100d.txt',
										'embeddings/datastories.twitter.200d.txt',
										'embeddings/datastories.twitter.300d.txt'],
				 otherMetricsToDisplay=[],
				 classWeightsSmoothingCoefficient=0):
		self.train_data_files = train_data_files
		self.val_data_files = val_data_files
		self.MAXEPOCHS = MAXEPOCHS
		self.logs_folder_path = logs_folder_path
		os.makedirs(self.logs_folder_path, exist_ok=True)
		self.models_folder_path = models_folder_path
		os.makedirs(self.models_folder_path, exist_ok=True)
		self.early_stopping_delta = early_stopping_delta
		self.early_stopping_patience = early_stopping_patience
		self.embeddings = self.loadEmbeddings(embeddings_file_paths)
		self.maxSequenceLength = maxSequenceLength
		self.batchSize = batchSize
		self.AccuracyFScoreAlpha = AccuracyFScoreAlpha
		self.textProcessorInstance = textProcessorInstance
		self.otherMetricsToDisplay=otherMetricsToDisplay
		self.classWeightsSmoothingCoefficient=classWeightsSmoothingCoefficient

	def loadEmbeddings(self, embeddingsFilePaths):
		embeddings = {}
		print('Loading Embeddings files...')
		for file_path in tqdm(embeddingsFilePaths):
			embeddings[file_path]=loadEmbeddings(file_path)
		return embeddings

	def countLabel(self, labelsDS, labelIdx):
		count = labelsDS.apply(tf.data.experimental.unbatch()).filter(lambda label_idx: tf.equal(label_idx, labelIdx)).reduce(
			0, lambda old_state, input: old_state + 1)
		return count
	def calculateSmoothedClassWeights(self, labelsDS, smoothingCoefficient):
		#countsTensor = labelsDS.apply(tf.data.experimental.unbatch()).reduce(tf.zeros((1,3), dtype=tf.int32), lambda labelSample, accumulatedValue: labelSample + accumulatedValue)
		#neagtive_count = labelsDS.apply(tf.data.experimental.unbatch()).filter(lambda label_idx: tf.equal(label_idx, 0)).reduce(0, lambda old_state, input: old_state + 1)
		countsTensor = tf.convert_to_tensor([self.countLabel(labelsDS, 0), self.countLabel(labelsDS, 1), self.countLabel(labelsDS, 2)], dtype=tf.float32)
		max_count = tf.reduce_max(countsTensor)
		weights_tensor = tf.squeeze(max_count / (countsTensor + smoothingCoefficient * max_count))
		return weights_tensor

	#@use_named_args
	def __call__(self, embedding_file_path, embeddingTrainable, embeddingNoiseStandardDeviation, embeddingDropout,
				bidirectional, nLayers, layersCellSize, RNNCellType, recurrentConnectionDropout,
				layerOutputDropout, attentionNumberOfLayers, attentionNumberOfNeuronsPerLayer,
				attentionLayerActivationFunction, l2RegularizationPenalityWeight, optimizerLearningRate,
				optimizerType, clipNorm):
		# make the date generation
		experiment_start_time = time.strftime("%Y_%m_%d_%H:%M:%S")
		# preparing the train and validation datasets
		start_making_datasets_time = time.time()
		wordToEmbeddingIndex, embeddingMatrix = self.embeddings[embedding_file_path]
		train_data = MyDataset(filePaths=self.train_data_files,
							   maxSequenceLength=self.maxSequenceLength,
							   batchSize=self.batchSize,
							   textProcessor=self.textProcessorInstance,
							   wordToEmbeddingIndex=wordToEmbeddingIndex)
		#trainInputs = train_data.map(lambda a, b: a)
		trainLabels = train_data.map(lambda a, b: b)
		val_data = MyDataset(filePaths=self.val_data_files,
							   maxSequenceLength=self.maxSequenceLength,
							   batchSize=self.batchSize,
							   textProcessor=self.textProcessorInstance,
							   wordToEmbeddingIndex=wordToEmbeddingIndex)
		lossesWeightsPerClass = self.calculateSmoothedClassWeights(trainLabels, self.classWeightsSmoothingCoefficient)
		end_making_datasets_time = time.time()
		print('Making datasets took: {:.2f}s'.format(end_making_datasets_time - start_making_datasets_time))
		# making the model
		model = createModel(experiment_start_time=experiment_start_time,
							embeddingFile=embedding_file_path,
							embeddingMatrix=embeddingMatrix,
							embeddingTrainable=embeddingTrainable,
							embeddingNoiseStandardDeviation=embeddingNoiseStandardDeviation,
							embeddingDropout=embeddingDropout,
							bidirectional=bidirectional,
							nLayers=nLayers,
							layersCellSize=layersCellSize,
							RNNCellType=RNNCellType,
							recurrentConnectionDropout=recurrentConnectionDropout,
							layerOutputDropout=layerOutputDropout,
							attentionNumberOfLayers=attentionNumberOfLayers,
							attentionNumberOfNeuronsPerLayer=attentionNumberOfNeuronsPerLayer,
							attentionLayerActivationFunction=attentionLayerActivationFunction,
							l2RegularizationPenalityWeight=l2RegularizationPenalityWeight,
							optimizerLearningRate=optimizerLearningRate,
							optimizerType=optimizerType,
							AccuracyFScoreAlpha=self.AccuracyFScoreAlpha,
							otherMetricsToDisplay=self.otherMetricsToDisplay,
							lossesWeightsPerClass=lossesWeightsPerClass,
							clipNorm=clipNorm)
		# training the model
		score = trainModel(experiment_start_time=experiment_start_time,
				   model=model,
				   train_data=train_data,
				   val_data=val_data,
				   lossesWeightsPerClass=lossesWeightsPerClass,
				   MAXEPOCHS=self.MAXEPOCHS,
				   logs_folder_path=self.logs_folder_path,
				   models_folder_path=self.models_folder_path,
				   early_stopping_delta=self.early_stopping_delta,
				   early_stopping_patience=self.early_stopping_patience)
		return score