#from preprocessData import preprocessAndSaveDataWithConfiguration1
import yaml
import argparse

from model import FitnessObject

def runExp1(config):
    #preprocessAndSaveDataWithConfiguration1()
    fitnessObject = FitnessObject(**config['training-parameters'])
    final_score = fitnessObject(**config['first-model-parameters'])
    print('final_score: {}'.format(final_score))






if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Training sentiment analysis.')
    parser.add_argument('--config', type=str, default='configs/config1.yaml', help='Path to experiment config.')
    paras = parser.parse_args()
    with open(paras.config, mode='r') as yaml_config_file:
        config = yaml.load(yaml_config_file)
    runExp1(config)