from tqdm import tqdm
import pandas as pd
import numpy as np

import tensorflow as tf
from tensorflow.data import Dataset
from tensorflow.data.experimental import AUTOTUNE

import sys
sys.path.insert(0, '..')
from Utils import loadData

from preprocessData import getTextProcessorWithConfiguration1

def padSentenceAndConvertToEmbeddings(sentence, textProcessor, maxSequenceLength, padIndex, unkIndex, wordToEmbeddingIndex):
    preprocessedTokens = textProcessor.pre_process_doc(sentence)
    #print('preprocessedTokens Lens: {}'.format(len(preprocessedTokens)))
    preprocessedTokensTrimmed = preprocessedTokens[:maxSequenceLength]
    featureIndices = [wordToEmbeddingIndex.get(word, unkIndex) for word in preprocessedTokensTrimmed]
    remainingLength = maxSequenceLength - len(preprocessedTokensTrimmed)
    featureIndices = featureIndices + [padIndex] * remainingLength
    return featureIndices


    #remainingLength = maxSequenceLength
def MyDataset(filePaths, maxSequenceLength, batchSize, textProcessor, wordToEmbeddingIndex):
    sentences, labels = loadData(filePaths)
    labelsDS = Dataset.from_tensor_slices(labels)
    #labelsDS = labelsDS.map(lambda label: tf.cast([tf.equal(label,'negative'), tf.equal(label,'neutral'), tf.equal(label, 'positive')], tf.int32), num_parallel_calls=AUTOTUNE)
    labelsDS = labelsDS.map(lambda label: 0 * tf.cast(tf.equal(label,'negative'), tf.int32) + 1 * tf.cast(tf.equal(label,'neutral'), tf.int32) + 2 * tf.cast(tf.equal(label, 'positive'), tf.int32))
                            # sentencesDS = Dataset.from_tensor_slices(sentences)
    unkIndex = wordToEmbeddingIndex['<unk>']
    padIndex = wordToEmbeddingIndex['<pad>']
    sentencesFeatures = list(map(lambda sentence: padSentenceAndConvertToEmbeddings(sentence, textProcessor, maxSequenceLength, padIndex, unkIndex, wordToEmbeddingIndex), sentences))
    sentencesDS = Dataset.from_tensor_slices(sentencesFeatures)
    finalDS = Dataset.zip((sentencesDS, labelsDS)).shuffle(buffer_size=len(sentences),
                                                           seed=7,
                                                           reshuffle_each_iteration=True
                                                           ).batch(batchSize)
    return finalDS

def loadEmbeddings(embeddingsFilePath):
    # dataFrame = pd.read_csv(embeddingsFilePath, sep=' ', header=None, index_col=0)
    # words = list(dataFrame.index)
    # wordIndices = np.arange(len(words))
    # wordToEmbeddingIdx = {word: idx for idx, word in zip(wordIndices, words)}
    # embeddingMatrix = dataFrame.as_matrix()
    word_counter = 0
    wordToEmbeddingIdx = {}
    embeddingMatrix = []
    with open(embeddingsFilePath, mode='r') as f:
        #lines = f.readlines()
        for line in f:
            tokens = line.split(' ')
            word = tokens[0]
            features = [float(num) for num in tokens[1:]]
            embeddingMatrix.append(features)
            wordToEmbeddingIdx[word] = word_counter
            word_counter += 1
    embeddingMatrix = np.array(embeddingMatrix, dtype=np.float)
    return wordToEmbeddingIdx, embeddingMatrix


def unitTestingFunctions():
    embeddingsFilePath = 'embeddings/datastories.twitter.50d.txt'
    wordToEmbeddingIdx, embeddingMatrix = loadEmbeddings(embeddingsFilePath)

    dataFilesPaths = ['../../Data/train-dev-test/twitter-2013train-A.txt',
                      '../../Data/train-dev-test/twitter-2015train-A.txt',
                      '../../Data/train-dev-test/twitter-2016train-A.txt']
    maxSequenceLength = 50
    batchSize = 64
    textPreprocessor = getTextProcessorWithConfiguration1()
    datasetInstance = MyDataset(dataFilesPaths, maxSequenceLength, batchSize, textPreprocessor, wordToEmbeddingIdx)
    print('Finished Testing')

if __name__ == '__main__':
    unitTestingFunctions()