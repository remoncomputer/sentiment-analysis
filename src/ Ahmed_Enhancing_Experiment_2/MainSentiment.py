
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
import Features
import re
import string
import nltk

def getStopWordList(stopWordFileName):
    #Membaca file stopword dan membuatnya menjadi list
    stopWords = []
    stopWords.append('at_user')
    stopWords.append('url')
    
    fp = open(stopWordFileName, 'r')
    line = fp.readline()
    while line:
        word = line.strip()
        stopWords.append(word)
        line = fp.readline()
    fp.close()
    return stopWords

def loadSlangs(slangsFileName):
    slangs = {}
    fi = open(slangsFileName,'r')
    line = fi.readline()
    while line:
        l = line.split(r',%,')
        if len(l) == 2:
            slangs[l[0]] = l[1][:-1]
        line = fi.readline()
    fi.close()
    return slangs

def slangWord(tweet):
    result = ''
    words = tweet.split()
    for w in words:
        if w in slangs.keys():
            result=result+slangs[w]+" "
        else:
            result=result+w+" "
    return result

def negation_tag(sentence):
    transformed = re.sub(r'\b(?:not|never|no|none|nothing|nowhere|noone|no|nobody|nowhere|neither|no one|nothing|hardly|scarcely|barely)\b[\w\s]+[^\w\s?]', 
       lambda match: re.sub(r'(\s+)(\w+)', r'\1NEG_\2', match.group(0)), 
       sentence,
       flags=re.IGNORECASE)
    
    return transformed


def removeUrl(text):
    link_regex    = re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~?\+-=\\\.&](#!)?)*)', re.DOTALL)
    links         = re.findall(link_regex, text)
    for link in links:
        text = text.replace(link[0], ', ')
    text = re.sub("\.\.\.", "", text)
    return text

def removePunctuation(text):
    remove = string.punctuation
    remove = remove.replace("_", "") # don't remove hyphens
    pattern = r"[{}]".format(remove) # create the pattern

    remoPunct = re.sub(pattern, "", text)
    return remoPunct

def removeStopword(words):
    processedTweet = ''
    for w in words:
        #Strip punctuation
        if w in stopWords:
            None
        else:
            w = w.replace('''"''', ''' ''')
            processedTweet = processedTweet+w+' '
    return processedTweet

def lemmatize(line):
    lemma = nltk.stem.WordNetLemmatizer()
    line = line.split()
    sentence = []
    for word in line:
        temp = lemma.lemmatize(word)
        if temp == word:
            temp = lemma.lemmatize(word,'v')
        sentence.append(temp)
    sentence = ' '.join(sentence)
    return sentence

def hashtag(tweet):
    result = ''
    for w in tweet.split():
        if w[0] == '#' :
            result = result+w[1:]+" "
        else:
            result = result+w+" "
    return result
    
def Tag(text):
    result = ''
    for w in text.split():
        if w[0] == '@' :
            result = result+w[1:]+" "
        else:
            result = result+w+" "
    return result
    
def preprocessing(line):
    line = slangWord(line)
    line = negation_tag(line)
    line = removeUrl(line)
    line = hashtag(line)
    line = Tag(line)
    line = removePunctuation(line)
    line = line.lower()
    line = line.split()
    line = removeStopword(line)
    line = lemmatize(line)
    return line

def createCorpus(data):
    print ("Process Create Corpus")
    listCorpus = []
    for line in data:
        try:
            line = preprocessing(line)
            listCorpus.append(line)
        except:
            None
    return listCorpus
