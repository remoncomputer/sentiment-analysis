from sklearn.metrics import accuracy_score, f1_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer # maybe I should examin other vectorizers
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.ensemble import BaggingClassifier, GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier

from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

import pandas as pd
import time
import yaml
import pickle as pkl
import re
import os
import sys

from ekphrasis.classes.preprocessor import TextPreProcessor
from ekphrasis.classes.tokenizer import SocialTokenizer
from ekphrasis.dicts.emoticons import emoticons


sys.path.insert(0, '..')
from Utils import loadData, loadStopWords, loadLexicons, splitData, generateSubmissionFile

PORTER_STEMMER = PorterStemmer()
def light_preprocessor(sentence):
    tokens = re.split("\W+", sentence) # tokenizer
    tokens_after_preprocessing = []
    for token in tokens:
        if len(token) == 0:
            continue
        token = token.lower()
        token = PORTER_STEMMER.stem(token)
        tokens_after_preprocessing.append(token)
    return ' '.join(tokens_after_preprocessing)

def loadFeatureSelectionWords(feature_method_names=[
                                                    'chi2',
                                                    'f_classif',
                                                    'mutual_info_classif',
                                                    'recursive_feature_elamination_with_cross_validation'
],
                              top_n_selections=[100, 500, 1000, 2000, 5000, 10000], #
                              feature_selection_directory='feature_selection'):
    feature_names_vs_word_features=[]
    for feature_method_name in feature_method_names:
        features_path = os.path.join(feature_selection_directory, '{}.csv'.format(feature_method_name))
        dataFrame = pd.read_csv(features_path, index_col=None)
        words_list_sorted = list(dataFrame['feature_name'])
        for selection_n in top_n_selections:
            feature_name ='{}_top_{}_words'.format(feature_method_name, selection_n)
            feature_words = words_list_sorted[:selection_n]
            feature_names_vs_word_features.append((feature_name, feature_words))
    return feature_names_vs_word_features

def train_sklearn_classifier_and_save_it_and_its_results(train_data, test_data, classifier, output_classifer_path_without_extension, extra_training_data=None):
    time_prefix = time.strftime('%Y-%m-%d_%H:%M:%S')
    train_x = train_data[0]
    train_y = train_data[1]
    using_extra_training_data = extra_training_data is not None
    if using_extra_training_data:
        train_x.extend(extra_training_data[0])
        train_y.extend(extra_training_data[1])
    classifier.fit(X=train_x, y=train_y)
    predicted_train = classifier.predict(train_x)
    test_x = test_data[0]
    test_y = test_data[1]
    predicted_test = classifier.predict(test_x)
    train_accuracy = accuracy_score(train_y, predicted_train)
    print('Train accuracy: {}'.format(train_accuracy))
    test_accuracy = accuracy_score(test_y, predicted_test)
    print('Test accuracy: {}'.format(test_accuracy))
    train_f_score = f1_score(train_y, predicted_train, average='macro')
    print('Train f1-score: {}'.format(train_f_score))
    test_f1_score = f1_score(test_y, predicted_test, average='macro')
    print('Test f1-score: {}'.format(test_f1_score))
    statistics_dict = {'training':
                           {'accurcay':float(train_accuracy),
                            'f1':float(train_f_score)},
                       'test':{'accuracy':float(test_accuracy),
                               'f1': float(test_f1_score)
                               }
                       }
    statistics_file_path = '{}-{}-results.yaml'.format(output_classifer_path_without_extension, time_prefix)
    with open(statistics_file_path, mode='w') as f:
        yaml.dump(statistics_dict, f, default_flow_style=False)
    model_file_path = '{}-{}-model.pkl'.format(output_classifer_path_without_extension, time_prefix)
    with open(model_file_path, mode='wb') as f:
        pkl.dump(classifier, f)

NLTK_STOP_WORDS =  set(stopwords.words("english"))

def runExp1():
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer', CountVectorizer(vocabulary=feature_words, binary=True, stop_words=NLTK_STOP_WORDS)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                        '../../Data/twitter-2015train.txt',
                                                        '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_count_vectorizer_logestic_regression')
    generateSubmissionFile('../../Data/test.csv',
                           'output/output_count_vectorizer_logestic_regression_submission.csv',
                           pipeline)

def runExp2():
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer', CountVectorizer(vocabulary=feature_words, binary=False, lowercase=True, stop_words=NLTK_STOP_WORDS)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                        '../../Data/twitter-2015train.txt',
                                                        '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_count_vectorizer_logestic_regression_with_counts')
    generateSubmissionFile('../../Data/test.csv',
                           'output/count_vectorizer_logestic_regression_submission_with_counts.csv',
                           pipeline)

def runExp3():
    feature_words = loadFeatureSelectionWords(feature_method_names=['f_classif'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer', CountVectorizer(preprocessor=light_preprocessor, vocabulary=feature_words, binary=False, lowercase=True)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                        '../../Data/twitter-2015train.txt',
                                                        '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/f_classif_output_count_vectorizer_logestic_regression_with_counts_with_stemmer')
    generateSubmissionFile('../../Data/test.csv',
                           'output/f_classif_count_vectorizer_logestic_regression_submission_with_counts_with_stemmer.csv',
                           pipeline)

default_text_processor = TextPreProcessor(
        # terms that will be normalized
        normalize=['url', 'email', 'percent', 'money', 'phone', 'user',
                   'time', 'url', 'date', 'number'],
        # terms that will be annotated
        annotate={"hashtag", "allcaps", "elongated", "repeated",
                  'emphasis', 'censored'},
        fix_html=True,  # fix HTML tokens

        # corpus from which the word statistics are going to be used
        # for word segmentation
        segmenter="twitter",

        # corpus from which the word statistics are going to be used
        # for spell correction
        corrector="twitter",

        unpack_hashtags=True,  # perform word segmentation on hashtags
        unpack_contractions=True,  # Unpack contractions (can't -> can not)
        spell_correct_elong=False,  # spell correction for elongated words

        # select a tokenizer. You can use SocialTokenizer, or pass your own
        # the tokenizer, should take as input a string and return a list of tokens
        tokenizer=SocialTokenizer(lowercase=True).tokenize,

        # list of dictionaries, for replacing tokens extracted from the text,
        # with other expressions. You can pass more than one dictionaries.
        dicts=[emoticons]
    )

def preprocess_sentence_ekphrasis(sentence):
    # if textprocessor is None:
    textprocessor = default_text_processor
    return ' '.join(textprocessor.pre_process_doc(sentence))

def runExp4():
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer', CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, vocabulary=feature_words, binary=False, lowercase=False)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                        '../../Data/twitter-2015train.txt',
                                                        '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/chi2_output_count_vectorizer_logestic_regression_with_counts_with_ekphrasis')
    generateSubmissionFile('../../Data/test.csv',
                           'output/chi2_count_vectorizer_logestic_regression_submission_with_counts_with_ekphrasis.csv',
                           pipeline)

def runExp4():
    #feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer', CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, ngram_range=(1,2), binary=False, lowercase=True)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                        '../../Data/twitter-2015train.txt',
                                                        '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_count_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_lowercase')
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_lowercase.csv',
                           pipeline)

# This is the best experiment with score=0.88 on the leader board
def runExp5():
    #feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer', CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, binary=False, lowercase=False)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                        '../../Data/twitter-2015train.txt',
                                                        '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2013test-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2015test-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2016dev-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2016test-A.txt'
    ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_count_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_with_extra_data',
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_with_extra_data.csv',
                           pipeline)

#Wow a score 95% is really unbelievable
def runExp6():
    #feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer', CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, ngram_range=(1,2), binary=False, lowercase=False)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                        '../../Data/twitter-2015train.txt',
                                                        '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2013test-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2015test-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2016dev-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2016test-A.txt'
    ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_count_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_bi_gram',
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_bigram.csv',
                           pipeline)

def runExp7():
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer', CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, vocabulary=feature_words, ngram_range=(1,2), binary=False, lowercase=False)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                        '../../Data/twitter-2015train.txt',
                                                        '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2013test-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2015test-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2016dev-A.txt',
                                                                       '../../Data/train-dev-test/twitter-2016test-A.txt'
    ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_count_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_bi_gram_with_chi',
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_bigram_with_chi.csv',
                           pipeline)

def runExp8():
    classifier_name = 'TFIDF_vectorizer_logestic_regression_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_chi_without_test'
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer',
                          TfidfVectorizer(preprocessor=preprocess_sentence_ekphrasis, vocabulary=feature_words,
                                          ngram_range=(1, 2), binary=False, lowercase=False)),
                         ('classifier', LogisticRegression())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                           '../../Data/twitter-2015train.txt',
                                                           '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'
                        ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_'+classifier_name,
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_'+classifier_name+'.csv',
                           pipeline)

def runExp9():
    classifier_name = 'TFIDF_vectorizer_Multinomial_naive_bays_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_chi_without_test'
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer',
                          TfidfVectorizer(preprocessor=preprocess_sentence_ekphrasis, vocabulary=feature_words,
                                          ngram_range=(1, 2), binary=False, lowercase=False)),
                         ('classifier', MultinomialNB())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                           '../../Data/twitter-2015train.txt',
                                                           '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'
                        ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_'+classifier_name,
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_'+classifier_name+'.csv',
                           pipeline)

def runExp10():
    classifier_name = 'count_vectorizer_with_linear_svm_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_chi_without_test'
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer',
                          CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, vocabulary=feature_words,
                                          ngram_range=(1, 2), binary=False, lowercase=False)),
                         ('classifier', LinearSVC())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                           '../../Data/twitter-2015train.txt',
                                                           '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'
                        ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_'+classifier_name,
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_'+classifier_name+'.csv',
                           pipeline)

def runExpEnsemble1():
    classifier_name = 'count_vectorizer_random_forest_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_chi_without_test'
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer',
                          CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, vocabulary=feature_words,
                                          ngram_range=(1, 2), binary=False, lowercase=False)),
                         ('classifier', RandomForestClassifier())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                           '../../Data/twitter-2015train.txt',
                                                           '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'
                        ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_' + classifier_name,
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_' + classifier_name + '.csv',
                           pipeline)

def runExpEnsemble2():
    classifier_name = 'count_vectorizer_adaa_boost_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_chi_without_test'
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer',
                          CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, vocabulary=feature_words,
                                          ngram_range=(1, 2), binary=False, lowercase=False)),
                         ('classifier', AdaBoostClassifier())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                           '../../Data/twitter-2015train.txt',
                                                           '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'
                        ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_' + classifier_name,
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_' + classifier_name + '.csv',
                           pipeline)

def runExpEnsemble3():
    classifier_name = 'count_vectorizer_bagging_logestic_regression_of_with_counts_with_ekphrasis_with_bi_gram_with_extra_data_with_chi_without_test'
    feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer',
                          CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, vocabulary=feature_words,
                                          ngram_range=(1, 2), binary=False, lowercase=False)),
                         ('classifier', BaggingClassifier(LogisticRegression(), n_estimators=50))])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                           '../../Data/twitter-2015train.txt',
                                                           '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'
                        ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_' + classifier_name,
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_count_' + classifier_name + '.csv',
                           pipeline)

def runExpEnsemble4():
    classifier_name = 'count_vectorizer_bagging_KNN_of_with_counts_with_ekphrasis_with_extra_data_without_test'
    #feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer',
                          CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, #vocabulary=feature_words,
                                          #ngram_range=(1, 2),
                                          binary=False, lowercase=False)),
                         ('classifier', BaggingClassifier(KNeighborsClassifier(),  max_samples=0.5, max_features=0.5))])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                           '../../Data/twitter-2015train.txt',
                                                           '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'
                        ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_' + classifier_name,
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_' + classifier_name + '.csv',
                           pipeline)

def runExpEnsemble5():
    classifier_name = 'count_vectorizer_gradient_boosting_of_with_counts_with_ekphrasis_with_extra_data_without_test'
    #feature_words = loadFeatureSelectionWords(feature_method_names=['chi2'], top_n_selections=[10000])[0][1]
    pipeline = Pipeline([('vectorizer',
                          CountVectorizer(preprocessor=preprocess_sentence_ekphrasis, #vocabulary=feature_words,
                                          #ngram_range=(1, 2),
                                          binary=False, lowercase=False)),
                         ('classifier', GradientBoostingClassifier())])
    data_sentences, data_labels = loadData(dataFilesPaths=['../../Data/twitter-2013train.txt',
                                                           '../../Data/twitter-2015train.txt',
                                                           '../../Data/twitter-2016train.txt'])
    train_sentences, test_sentences, train_labels, test_labels = splitData(data_sentences, data_labels)
    extra_data_sentences, extra_data_labels = loadData(
        dataFilesPaths=['../../Data/train-dev-test/twitter-2013dev-A.txt',
                        '../../Data/train-dev-test/twitter-2016dev-A.txt'
                        ])
    train_sklearn_classifier_and_save_it_and_its_results(train_data=(train_sentences, train_labels),
                                                         test_data=(test_sentences, test_labels),
                                                         classifier=pipeline,
                                                         output_classifer_path_without_extension='output/output_' + classifier_name,
                                                         extra_training_data=(extra_data_sentences, extra_data_labels))
    generateSubmissionFile('../../Data/test.csv',
                           'output/submission_' + classifier_name + '.csv',
                           pipeline)

if __name__ == '__main__':
    #runExp1()
    runExpEnsemble5()